<?php
/**
 * Created by PhpStorm.
 * User: AlexS
 * Date: 03.02.2019
 * Time: 16:29
 */
require_once __DIR__.'/Helper.php';
class Speed extends Helper {
    /**
     * @param float $speed
     * @param string $mode
     * @return float
     * @throws Exception
     */
    public static function calculate ($speed, $mode){
        if ((empty($speed)||(empty($mode)))){
            throw new Exception('Не заданы входные данные');
        }
        if (!floatval($speed)){
            throw new Exception('скорость задана некорректно');
        }

        if ($mode==='KMPH'){
            $speed = floatval($speed)/3.6.' m/s';
        } else if ($mode==="MPS"){
            $speed = floatval($speed)*3.6.' km/h';
        } else {
            throw new Exception('неправильный режим');
        }

        return $speed;
    }

}