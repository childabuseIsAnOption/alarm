<?php

class Helper {
    /**
     * @param array $result
     * @throws Exception
     */
    public static function returnJSON (array $result){
        if (empty($result)){
            throw new Exception('не заданы входные данные');
        }
        header('Content-Type: application/json');
        echo json_encode($result);
    }
}