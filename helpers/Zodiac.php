<?php

require_once __DIR__.'/Helper.php';

class Zodiac extends Helper {

    /**
     * @param string $date
     * @return string
     * @throws Exception
     */
    public static function calculateZodiacSign ($date){
        if(empty($date)){
            throw new Exception('не заданы входные данные');
        }
        if ((strpos($date,'-')!=4)&&(strpos(substr($date,0,5),'-'))!=2){
            throw new Exception('некорректный формат даты');
        }
        $day = intval(substr($date, 8, 2));
        $month = intval(substr($date, 6, 2));
        switch ($month){
            case 1:
                if ($day>=21){
                    $result = 'Водолей';
                } else {
                    $result = 'Козерог';
                }
                break;
            case 2:
                if ($day>=21){
                    $result = 'Рыбы';
                } else {
                    $result = 'Водолей';
                }
                break;
            case 3:
                if ($day>=21){
                    $result = 'Овен';
                } else {
                    $result = 'Рыбы';
                }
                break;
            case 4:
                if ($day>=21){
                    $result = 'Телец';
                } else {
                    $result = 'Овен';
                }
                break;
            case 5:
                if ($day>=21){
                    $result = 'Близнецы';
                } else {
                    $result = 'Телец';
                }
                break;
            case 6:
                if ($day>=22){
                    $result = 'Рак';
                } else {
                    $result = 'Близнецы';
                }
                break;
            case 7:
                if ($day>=23){
                    $result = 'Лев';
                } else {
                    $result = 'Рак';
                }
                break;
            case 8:
                if ($day>=24){
                    $result = 'Дева';
                } else {
                    $result = 'Лев';
                }
                break;
            case 9:
                if ($day>=24){
                    $result = 'Весы';
                } else {
                    $result = 'Дева';
                }
                break;
            case 10:
                if ($day>=24){
                    $result = 'Скорпион';
                } else {
                    $result = 'Весы';
                }
                break;
            case 11:
                if ($day>=23){
                    $result = 'Стрелец';
                } else {
                    $result = 'Скорпион';
                }
                break;
            case 12:
                if ($day>=22){
                    $result = 'Козерог';
                } else {
                    $result = 'Стрелец';
                }
                break;
        }
        return $result;
    }


}