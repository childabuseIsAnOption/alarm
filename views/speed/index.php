<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="../../css/main.css">
</head>
<body>
<form>
    <div class="col">
        <label for="kmph">Input speed value:</label><br>
        <input type="text" id="speed" name="speed"><br>
        <input type="radio" name="mode" value="KMPH" id="kmph" checked>
        <label for="kmph">Speed in KM per H:</label>
        <br>
        <input type="radio" name="mode" id="mps" value="MPS">
        <label for="mps">Speed in M per S:</label>
        <br>

        <input type="submit" value="calculate">
    </div>
    <div class="col">
        <label for="result">RESULT OF CALCULATION:</label><br>
        <input type="text" id="result">
    </div>
</form>
<a href="/" class="button">вернуться</a>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    $(document).ready(function(){
       $(document).on('click','input[type="submit"]', function (e) {
           e.preventDefault();
           var mode = $('input[name=mode]:checked').val();
           var speed = $('#speed').val();
           $.ajax ({
               'url':'../../ajax/speed.php',
               'type':'POST',
               'data':{
                   speed:speed,
                   mode:mode
               },
               success:function(data){
                    console.log(data);
                    $('#result').val(data.result);
               }
               });
       })
    });
</script>
</body>
</html>


