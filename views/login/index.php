<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="/css/main.css">

</head>
<body>
<div class="container">
    <div class="loginForm">
        <form class="form">
            <input type="text" name="login" placeholder="your login" class="login"><br>
            <input type="password" name="password" placeholder="your password" class="password"><br>
            <input type="submit" value="go!" class="submit">
        </form>
        <div class="success">
            Вы успещно вошли
        </div>
    </div>
</div>
<a href="/" class="button">вернуться</a>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    $(document).ready(function () {
        $(document).on('click', '.submit', function (e) {
            e.preventDefault();
            $('.form').hide();
            $('.success').fadeIn(300);

        })
    });
</script>
</body>
</html>