<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="../../css/main.css">
</head>
<body>
<form method="get" action="/views/zodiac/index.php">
    <div class="col">
        <label for="date">Input date of birth:</label><br>
        <input type="date" name="date">
        <input type="submit" value="calculate">
    </div>
    <div class="col">
        <label for="output">Your zodiac sign is:</label>
        <input type="text" id="output">
    </div>
</form>
<a href="/" class="button">вернуться</a>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    $(document).ready(function () {
       $(document).on('click', 'input[type="submit"]', function (e) {
           e.preventDefault();
           var date = $('input[name="date"]').val();
           $.ajax({
               'url':'../../ajax/zodiac.php',
               'type':'POST',
               'data':{
                   date:date
               },
               success: function (data) {
                    console.log(data);
                    $('#output').val(data.result);
               }
           })
       })
    });
</script>
</body>
</html>