<?php
/**
 * Created by PhpStorm.
 * User: AlexS
 * Date: 03.02.2019
 * Time: 17:06
 */

require_once $_SERVER['DOCUMENT_ROOT'].'/helpers/Zodiac.php';

try {
    $zodiacSign = Zodiac::calculateZodiacSign($_REQUEST['date']);
    $result = [
        'stauts' => 'ok',
        'result' => $zodiacSign
    ];
} catch (Exception $e){
    $result = [
        'status' => 'error',
        'result' => $e->getMessage()
    ];
}

Zodiac::returnJSON($result);