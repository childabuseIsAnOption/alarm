<?php
/**
 * Created by PhpStorm.
 * User: AlexS
 * Date: 03.02.2019
 * Time: 16:27
 */
require_once $_SERVER['DOCUMENT_ROOT'].'/helpers/speed.php';
try {
    $speed = Speed::calculate($_REQUEST['speed'], $_REQUEST['mode']);
    $result = [
        'status' => 'ok',
        'result' => $speed
    ];
} catch (Exception $e) {
    $result = [
      'status' => 'error',
      'result' => $e->getMessage()
    ];
}
Speed::returnJSON($result);
